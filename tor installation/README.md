# set tor in ubuntu

install tor and git lab

## نصب تور

برای نصب تور که یک فیلتر شکن رایگان به حساب می اید مراحل زیر را طی کنید.

### تنظیم منطقه زمانی
به مکان زیر رفته و منطقه زمانی را بر روی ایران گذاشته و ساعت را به طور دستی درست کنید.

```
setting -> details -> date&time 
```

### نصب

در ترمینال دستورات زیر را به ترتیب وارد کنید.

```
sudo apt update
sudo apt install tor
sudo systemctl enable tor.service
sudo systemctl start tor.service
```
ترمینال رو باز می‌کنید و می‌نویسید:

```
cat /etc/systemd/system/multi-user.target.wants/tor.service
```

اگر خروجی این دستور به این شکل بود:
```
[Unit]
Description=Anonymizing overlay network for TCP (multi-instance-master)

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/bin/true
ExecReload=/bin/true

[Install]
WantedBy=multi-user.target
```
باید فایل مورد نظر را اصلاح کنید . در غیر این صورت از این مرحله بگذرید .
برای اصلاح فایل در ترمینال دستور زیر را وارد کنید:
```
sudo geany /etc/systemd/system/multi-user.target.wants/tor.service
```
میتوانید به جای geany از ویرایشگر های دیگر نیز استفاده کنید.
حال متن فایل را به متن زیر تغییر دهید:

```
[Unit]
Description=Anonymizing overlay network for TCP (multi-instance-master)

[Service]
User=debian-tor
Type=simple
RemainAfterExit=yes
ExecStart=/usr/bin/tor -f /etc/tor/torrc
ExecReload=/usr/bin/kill -HUP $MAINPID
KillSignal=SIGINT
LimitNOFILE=8192
PrivateDevices=yes

[Install]
WantedBy=multi-user.target
```
سپس دو دستور زیر را وارد کنید:
```
sudo systemctl daemon-reload
sudo systemctl restart tor.service
```


### استفاده از تور در ترمینال

ترمینال رو باز می‌کنید و می‌نویسید:
```
sudo apt install torsocks
less /etc/tor/torsocks.conf
```
و چک کنید این ۲ خط کامنت نباشن:
```
TorAddress 127.0.0.1
TorPort 9050
```

توی ترمینال هرجا تور خواستید قبل دستورتون می‌نویسید:
```
torsocks
```


### استفاده از تور در firefox

در فایر فاکس به مکان زیر بروید :

```
perference -> network setting 
```
بر روی Manual proxy configuration بگذارید . SOCKS Host را 127.0.0.1 بگذارید . و port را 9050 بگذارید.


