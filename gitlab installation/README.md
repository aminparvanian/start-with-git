# install git & git lab in ubuntu

 مراحل زیر را طی کنید.

## نصب گیت

دستورات زیر را در ترمینال وارد کنید:


```
sudo apt update
sudo apt install git
```

## نصب گیت لب

دستورات زیر را در ترمینال وارد کنید:

```
sudo apt update
sudo apt install curl
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo apt update
sudo apt install gitlab-ee
```

## کار با گیت لب

 * [work with gitlab & git in terminal - persina instruction ](http://mekaeil.me/farsi/git-command-archives)
